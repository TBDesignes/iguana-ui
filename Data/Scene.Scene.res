﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">33</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="4216951332">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2961610138">
        <_items dataType="Array" type="Duality.Component[]" id="1483008896" length="8">
          <item dataType="Struct" type="Duality.Components.Transform" id="4274228550">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <gameobj dataType="ObjectRef">4216951332</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-500</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-500</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="1468370513">
            <active dataType="Bool">true</active>
            <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
            <farZ dataType="Float">10000</farZ>
            <focusDist dataType="Float">500</focusDist>
            <gameobj dataType="ObjectRef">4216951332</gameobj>
            <nearZ dataType="Float">50</nearZ>
            <priority dataType="Int">0</priority>
            <projection dataType="Enum" type="Duality.Drawing.ProjectionMode" name="Perspective" value="1" />
            <renderSetup dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderSetup]]" />
            <renderTarget dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
            <shaderParameters dataType="Struct" type="Duality.Drawing.ShaderParameterCollection" id="2900698069" custom="true">
              <body />
            </shaderParameters>
            <targetRect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">1</H>
              <W dataType="Float">1</W>
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
            </targetRect>
            <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
          </item>
          <item dataType="Struct" type="Duality.Components.VelocityTracker" id="1993118503">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">4216951332</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.SoundListener" id="1954636563">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">4216951332</gameobj>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1377041210" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="4238141408">
            <item dataType="Type" id="3820800988" value="Duality.Components.Transform" />
            <item dataType="Type" id="3496175894" value="Duality.Components.VelocityTracker" />
            <item dataType="Type" id="4089388360" value="Duality.Components.Camera" />
            <item dataType="Type" id="1931878834" value="Duality.Components.SoundListener" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="3359552398">
            <item dataType="ObjectRef">4274228550</item>
            <item dataType="ObjectRef">1993118503</item>
            <item dataType="ObjectRef">1468370513</item>
            <item dataType="ObjectRef">1954636563</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">4274228550</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="540161788">p0Rlb90hz0+nVCX+VxNgtQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">MainCamera</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="506374765">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1045062623">
        <_items dataType="Array" type="Duality.Component[]" id="311134830" length="4">
          <item dataType="Struct" type="TBUI.MyController" id="2621626521">
            <_x003C_HoveredMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\HoveredMaterial.Material.res</contentPath>
            </_x003C_HoveredMaterial_x003E_k__BackingField>
            <_x003C_Material_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\DefaultMaterial.Material.res</contentPath>
            </_x003C_Material_x003E_k__BackingField>
            <_x003C_Source_x003E_k__BackingField dataType="Struct" type="TBUI.SourceUI" id="2847306784">
              <_x003C_camera_x003E_k__BackingField dataType="ObjectRef">1468370513</_x003C_camera_x003E_k__BackingField>
              <_x003C_Visibility_x003E_k__BackingField dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0, AllFlags" value="2147483649" />
              <active dataType="Bool">true</active>
              <canvas />
              <gameobj dataType="ObjectRef">506374765</gameobj>
              <hoveredNode />
              <root dataType="Struct" type="TBUI.Containers.StackPanel" id="1801381560">
                <_parent />
                <_x003C_Children_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.HashSet`1[[TBUI.Node]]" id="1130430633" surrogate="true">
                  <header />
                  <body>
                    <values dataType="Array" type="TBUI.Node[]" id="3355896846">
                      <item dataType="Struct" type="TBUI.Node" id="2228691920">
                        <_parent dataType="ObjectRef">1801381560</_parent>
                        <_x003C_Children_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.HashSet`1[[TBUI.Node]]" id="1164049084" surrogate="true">
                          <header />
                          <body>
                            <values dataType="Array" type="TBUI.Node[]" id="3352642116" length="0" />
                          </body>
                        </_x003C_Children_x003E_k__BackingField>
                        <_x003C_IsCursorHovering_x003E_k__BackingField dataType="Bool">false</_x003C_IsCursorHovering_x003E_k__BackingField>
                        <_x003C_Name_x003E_k__BackingField dataType="String">Button 0</_x003C_Name_x003E_k__BackingField>
                        <_x003C_Source_x003E_k__BackingField />
                        <_x003C_Style_x003E_k__BackingField dataType="Struct" type="TBUI.Style" id="512901782">
                          <_x003C_DefaultMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\DefaultMaterial.Material.res</contentPath>
                          </_x003C_DefaultMaterial_x003E_k__BackingField>
                          <_x003C_HoveredMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\HoveredMaterial.Material.res</contentPath>
                          </_x003C_HoveredMaterial_x003E_k__BackingField>
                        </_x003C_Style_x003E_k__BackingField>
                        <_x003C_Transform_x003E_k__BackingField dataType="Struct" type="TBUI.TransformUI" id="3501405800">
                          <actualRect dataType="Struct" type="Duality.Rect" />
                          <clippingRect dataType="Struct" type="Duality.Rect" />
                          <dirty dataType="Bool">true</dirty>
                          <localPosition dataType="Struct" type="Duality.Vector2" />
                          <size dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">80</X>
                            <Y dataType="Float">80</Y>
                          </size>
                        </_x003C_Transform_x003E_k__BackingField>
                        <enabled dataType="Bool">true</enabled>
                      </item>
                      <item dataType="Struct" type="TBUI.Node" id="3100103278">
                        <_parent dataType="ObjectRef">1801381560</_parent>
                        <_x003C_Children_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.HashSet`1[[TBUI.Node]]" id="1998849186" surrogate="true">
                          <header />
                          <body>
                            <values dataType="Array" type="TBUI.Node[]" id="876216080" length="0" />
                          </body>
                        </_x003C_Children_x003E_k__BackingField>
                        <_x003C_IsCursorHovering_x003E_k__BackingField dataType="Bool">false</_x003C_IsCursorHovering_x003E_k__BackingField>
                        <_x003C_Name_x003E_k__BackingField dataType="String">Button 1</_x003C_Name_x003E_k__BackingField>
                        <_x003C_Source_x003E_k__BackingField />
                        <_x003C_Style_x003E_k__BackingField dataType="Struct" type="TBUI.Style" id="3274097418">
                          <_x003C_DefaultMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\DefaultMaterial.Material.res</contentPath>
                          </_x003C_DefaultMaterial_x003E_k__BackingField>
                          <_x003C_HoveredMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\HoveredMaterial.Material.res</contentPath>
                          </_x003C_HoveredMaterial_x003E_k__BackingField>
                        </_x003C_Style_x003E_k__BackingField>
                        <_x003C_Transform_x003E_k__BackingField dataType="Struct" type="TBUI.TransformUI" id="4086426962">
                          <actualRect dataType="Struct" type="Duality.Rect" />
                          <clippingRect dataType="Struct" type="Duality.Rect" />
                          <dirty dataType="Bool">true</dirty>
                          <localPosition dataType="Struct" type="Duality.Vector2" />
                          <size dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">80</X>
                            <Y dataType="Float">80</Y>
                          </size>
                        </_x003C_Transform_x003E_k__BackingField>
                        <enabled dataType="Bool">true</enabled>
                      </item>
                      <item dataType="Struct" type="TBUI.Node" id="393919404">
                        <_parent dataType="ObjectRef">1801381560</_parent>
                        <_x003C_Children_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.HashSet`1[[TBUI.Node]]" id="4077679992" surrogate="true">
                          <header />
                          <body>
                            <values dataType="Array" type="TBUI.Node[]" id="1008758636" length="0" />
                          </body>
                        </_x003C_Children_x003E_k__BackingField>
                        <_x003C_IsCursorHovering_x003E_k__BackingField dataType="Bool">false</_x003C_IsCursorHovering_x003E_k__BackingField>
                        <_x003C_Name_x003E_k__BackingField dataType="String">Button 2</_x003C_Name_x003E_k__BackingField>
                        <_x003C_Source_x003E_k__BackingField />
                        <_x003C_Style_x003E_k__BackingField dataType="Struct" type="TBUI.Style" id="1085754334">
                          <_x003C_DefaultMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\DefaultMaterial.Material.res</contentPath>
                          </_x003C_DefaultMaterial_x003E_k__BackingField>
                          <_x003C_HoveredMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\HoveredMaterial.Material.res</contentPath>
                          </_x003C_HoveredMaterial_x003E_k__BackingField>
                        </_x003C_Style_x003E_k__BackingField>
                        <_x003C_Transform_x003E_k__BackingField dataType="Struct" type="TBUI.TransformUI" id="2968312868">
                          <actualRect dataType="Struct" type="Duality.Rect" />
                          <clippingRect dataType="Struct" type="Duality.Rect" />
                          <dirty dataType="Bool">true</dirty>
                          <localPosition dataType="Struct" type="Duality.Vector2" />
                          <size dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">80</X>
                            <Y dataType="Float">80</Y>
                          </size>
                        </_x003C_Transform_x003E_k__BackingField>
                        <enabled dataType="Bool">true</enabled>
                      </item>
                      <item dataType="Struct" type="TBUI.Node" id="3815084562">
                        <_parent dataType="ObjectRef">1801381560</_parent>
                        <_x003C_Children_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.HashSet`1[[TBUI.Node]]" id="1809297078" surrogate="true">
                          <header />
                          <body>
                            <values dataType="Array" type="TBUI.Node[]" id="541697888" length="0" />
                          </body>
                        </_x003C_Children_x003E_k__BackingField>
                        <_x003C_IsCursorHovering_x003E_k__BackingField dataType="Bool">false</_x003C_IsCursorHovering_x003E_k__BackingField>
                        <_x003C_Name_x003E_k__BackingField dataType="String">Button 3</_x003C_Name_x003E_k__BackingField>
                        <_x003C_Source_x003E_k__BackingField />
                        <_x003C_Style_x003E_k__BackingField dataType="Struct" type="TBUI.Style" id="4231445146">
                          <_x003C_DefaultMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\DefaultMaterial.Material.res</contentPath>
                          </_x003C_DefaultMaterial_x003E_k__BackingField>
                          <_x003C_HoveredMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\HoveredMaterial.Material.res</contentPath>
                          </_x003C_HoveredMaterial_x003E_k__BackingField>
                        </_x003C_Style_x003E_k__BackingField>
                        <_x003C_Transform_x003E_k__BackingField dataType="Struct" type="TBUI.TransformUI" id="224588118">
                          <actualRect dataType="Struct" type="Duality.Rect" />
                          <clippingRect dataType="Struct" type="Duality.Rect" />
                          <dirty dataType="Bool">true</dirty>
                          <localPosition dataType="Struct" type="Duality.Vector2" />
                          <size dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">80</X>
                            <Y dataType="Float">80</Y>
                          </size>
                        </_x003C_Transform_x003E_k__BackingField>
                        <enabled dataType="Bool">true</enabled>
                      </item>
                      <item dataType="Struct" type="TBUI.Node" id="3176942984">
                        <_parent dataType="ObjectRef">1801381560</_parent>
                        <_x003C_Children_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.HashSet`1[[TBUI.Node]]" id="431225940" surrogate="true">
                          <header />
                          <body>
                            <values dataType="Array" type="TBUI.Node[]" id="3421514980" length="0" />
                          </body>
                        </_x003C_Children_x003E_k__BackingField>
                        <_x003C_IsCursorHovering_x003E_k__BackingField dataType="Bool">false</_x003C_IsCursorHovering_x003E_k__BackingField>
                        <_x003C_Name_x003E_k__BackingField dataType="String">Button 4</_x003C_Name_x003E_k__BackingField>
                        <_x003C_Source_x003E_k__BackingField />
                        <_x003C_Style_x003E_k__BackingField dataType="Struct" type="TBUI.Style" id="3539784630">
                          <_x003C_DefaultMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\DefaultMaterial.Material.res</contentPath>
                          </_x003C_DefaultMaterial_x003E_k__BackingField>
                          <_x003C_HoveredMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                            <contentPath dataType="String">Data\HoveredMaterial.Material.res</contentPath>
                          </_x003C_HoveredMaterial_x003E_k__BackingField>
                        </_x003C_Style_x003E_k__BackingField>
                        <_x003C_Transform_x003E_k__BackingField dataType="Struct" type="TBUI.TransformUI" id="895401840">
                          <actualRect dataType="Struct" type="Duality.Rect" />
                          <clippingRect dataType="Struct" type="Duality.Rect" />
                          <dirty dataType="Bool">true</dirty>
                          <localPosition dataType="Struct" type="Duality.Vector2" />
                          <size dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">80</X>
                            <Y dataType="Float">80</Y>
                          </size>
                        </_x003C_Transform_x003E_k__BackingField>
                        <enabled dataType="Bool">true</enabled>
                      </item>
                    </values>
                  </body>
                </_x003C_Children_x003E_k__BackingField>
                <_x003C_IsCursorHovering_x003E_k__BackingField dataType="Bool">false</_x003C_IsCursorHovering_x003E_k__BackingField>
                <_x003C_Name_x003E_k__BackingField dataType="String">StackPanel</_x003C_Name_x003E_k__BackingField>
                <_x003C_Source_x003E_k__BackingField dataType="ObjectRef">2847306784</_x003C_Source_x003E_k__BackingField>
                <_x003C_Style_x003E_k__BackingField dataType="Struct" type="TBUI.Style" id="3067595200">
                  <_x003C_DefaultMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\DefaultMaterial.Material.res</contentPath>
                  </_x003C_DefaultMaterial_x003E_k__BackingField>
                  <_x003C_HoveredMaterial_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\HoveredMaterial.Material.res</contentPath>
                  </_x003C_HoveredMaterial_x003E_k__BackingField>
                </_x003C_Style_x003E_k__BackingField>
                <_x003C_Transform_x003E_k__BackingField dataType="Struct" type="TBUI.TransformUI" id="3868125835">
                  <actualRect dataType="Struct" type="Duality.Rect" />
                  <clippingRect dataType="Struct" type="Duality.Rect" />
                  <dirty dataType="Bool">true</dirty>
                  <localPosition dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">100</X>
                    <Y dataType="Float">100</Y>
                  </localPosition>
                  <size dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">150</X>
                    <Y dataType="Float">370</Y>
                  </size>
                </_x003C_Transform_x003E_k__BackingField>
                <_x003C_Vertical_x003E_k__BackingField dataType="Bool">true</_x003C_Vertical_x003E_k__BackingField>
                <enabled dataType="Bool">true</enabled>
              </root>
            </_x003C_Source_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">506374765</gameobj>
          </item>
          <item dataType="ObjectRef">2847306784</item>
        </_items>
        <_size dataType="Int">2</_size>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1810982176" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="4000955861">
            <item dataType="Type" id="3015126006" value="TBUI.SourceUI" />
            <item dataType="Type" id="1886057498" value="TBUI.MyController" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="684169288">
            <item dataType="ObjectRef">2847306784</item>
            <item dataType="ObjectRef">2621626521</item>
          </values>
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1754253791">+GY7YUHxmUOGU6ywpslK6w==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">SourceUI</name>
      <parent />
      <prefabLink />
    </item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->

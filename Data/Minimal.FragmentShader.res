﻿<root dataType="Struct" type="Duality.Resources.FragmentShader" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicShaderAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.frag</item>
    </sourceFileHint>
  </assetInfo>
  <source dataType="String">#pragma duality description "The main texture of the material."
uniform sampler2D mainTex;
uniform vec2 position;
uniform vec2 size;

in vec4 programColor;
in vec2 programTexCoord;
in vec4 gl_FragCoord;

out vec4 fragColor;

void main()
{
	vec4 texClr = texture(mainTex, programTexCoord);
	vec4 result = programColor * texClr;
	
	AlphaTest(result.a);
	if (gl_FragCoord.x &gt; position.x + size.x ||
		gl_FragCoord.x &lt; position.x ||
		gl_FragCoord.y &gt; position.y ||
		gl_FragCoord.y &lt; position.y - size.y)
		result.a = 0;
	//result.a = position.y / 661;
	fragColor = result;
}</source>
</root>
<!-- XmlFormatterBase Document Separator -->

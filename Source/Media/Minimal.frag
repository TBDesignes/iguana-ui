#pragma duality description "The main texture of the material."
uniform sampler2D mainTex;
uniform vec2 position;
uniform vec2 size;

in vec4 programColor;
in vec2 programTexCoord;
in vec4 gl_FragCoord;

out vec4 fragColor;

void main()
{
	vec4 texClr = texture(mainTex, programTexCoord);
	vec4 result = programColor * texClr;
	
	AlphaTest(result.a);
	if (gl_FragCoord.x > position.x + size.x ||
		gl_FragCoord.x < position.x ||
		gl_FragCoord.y > position.y ||
		gl_FragCoord.y < position.y - size.y)
		result.a = 0;
	//result.a = position.y / 661;
	fragColor = result;
}
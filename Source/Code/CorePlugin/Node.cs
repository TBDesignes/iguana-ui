﻿using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Drawing;
using Duality.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBUI {
    public class Node {
        private Node _parent;
        private bool enabled = true;

        public HashSet<Node> Children { get; } = new HashSet<Node>();
        public TransformUI Transform { get; set; } = new TransformUI();
        public string Name { get; set; }
        public SourceUI Source { get; set; }
        public bool IsCursorHovering { get; set; }
        public Style Style { get; set; }
        public bool IsCursorImageHovering { get; set; }
        public bool Enabled {
            get => enabled; set {
                enabled = value;
                if (enabled)
                    OnActivationChanged(ACTIVATION.Enabled);
                else
                    OnActivationChanged(ACTIVATION.Disabled);
            }
        }
        public Node Parent {
            get => _parent;
            set {
                if (value?.Parent == this || value == this) return;
                if (_parent != null) {
                    _parent.Children.Remove(this);
                    _parent.OnHierarchyChanged(HIERARCHY.Detached);
                }
                _parent = value;
                if (_parent != null) {
                    _parent.Children.Add(this);
                    _parent.OnHierarchyChanged(HIERARCHY.Attached);
                }
            }
        }

        public void FreeChildren() {
            while (Children.Count > 0) {
                Children.First().Parent = null;
            }
        }
        public SourceUI GetSource() {
            Node n = this;
            while (n != null || n.Source == null) {
                n = n.Parent;
            }
            return n.Source;
        }
        public void Drill(Action<Node> action) {
            if (Enabled) {
                action(this);
                foreach (var child in Children) {
                    child.Drill(action);
                }
            }
        }
        public virtual void OnDraw(Canvas canvas) {
            canvas.PushState();

            if (IsCursorHovering)
                canvas.State.SetMaterial(Style.HoveredMaterial);
            else
                canvas.State.SetMaterial(Style.DefaultMaterial);

            var pos = Transform.clippingRect.Pos;
            var mat = canvas.State.Material;
            mat.SetValue("position", new Vector2(pos.X, DualityApp.TargetViewSize.Y - pos.Y));
            mat.SetValue("size", Transform.clippingRect.Size);
            canvas.State.SetMaterial(mat);
            canvas.FillRect(Transform.actualRect.X, Transform.actualRect.Y, Transform.actualRect.W, Transform.actualRect.H);

            canvas.PopState();
        }

        public virtual void OnUpdate() {
            if (Transform.dirty)
                Drill(node => node.OnReconstruct());
        }

        public virtual void OnReconstruct() {
            Vector2 parentSize = Parent == null ? DualityApp.TargetViewSize : Parent.Transform.actualRect.Size;
            Vector2 parentPos = Parent == null ? Vector2.Zero : Parent.Transform.actualRect.Pos;
            Vector2 center = (Transform.Anchor1 + Transform.Anchor2) / 2;
            center.X *= parentSize.X;
            center.Y *= parentSize.Y;
            Transform.actualRect = new Rect(Transform.Size).WithOffset(parentPos + center - Transform.Size / 2 + Transform.Offset);
            Transform.clippingRect = Transform.actualRect;

            /*
            if (Parent == null) {
                Transform.actualRect = new Rect(Transform.Size).WithOffset(Transform.LocalPosition);
                Transform.clippingRect = Transform.actualRect;
            } else {
                Transform.actualRect = new Rect(Transform.Size).WithOffset(Transform.LocalPosition + Parent.Transform.actualRect.Pos);

                Transform.clippingRect.X = MathF.Clamp(Transform.actualRect.X, Parent.Transform.clippingRect.X, Parent.Transform.clippingRect.X + Parent.Transform.clippingRect.W);
                Transform.clippingRect.Y = MathF.Clamp(Transform.actualRect.Y, Parent.Transform.clippingRect.Y, Parent.Transform.clippingRect.Y + Parent.Transform.clippingRect.H);
                Transform.clippingRect.W = MathF.Clamp(Transform.actualRect.W, 0, MathF.Max(Parent.Transform.actualRect.W - Transform.LocalPosition.X, 0));
                Transform.clippingRect.H = MathF.Clamp(Transform.actualRect.H, 0, MathF.Max(Parent.Transform.actualRect.H - Transform.LocalPosition.Y, 0));

            }*/
            Transform.dirty = false;
        }

        public virtual void OnChildHierarchyChanged(Node child, HIERARCHY state) {
            OnReconstruct();
        }

        public virtual void OnChildActivationChanged(Node child, ACTIVATION state) {
            OnReconstruct();
        }

        public virtual void OnHierarchyChanged(HIERARCHY state) {
            Parent?.OnChildHierarchyChanged(this, state);
        }

        public virtual void OnActivationChanged(ACTIVATION state) {
            Parent?.OnChildActivationChanged(this, state);
            if (state == ACTIVATION.Disabled)
                IsCursorHovering = false;
        }

        public virtual void OnCursorHoverChanged(CURSOR_HOVER state) {
            Logs.Game.Write(Name + "  " + state);
        }
    }

    public enum HIERARCHY {
        Attached,
        Detached
    }

    public enum ACTIVATION {
        Enabled,
        Disabled
    }

    public enum CURSOR_HOVER {
        Entered,
        Exited
    }
}

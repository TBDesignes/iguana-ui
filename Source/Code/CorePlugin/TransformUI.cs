﻿using Duality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBUI {
    public class TransformUI {
        private Vector2 localPosition;
        private Vector2 size;
        public Vector2 Anchor1 { get; set; }
        public Vector2 Anchor2 { get; set; }
        public Vector2 Offset { get; set; }
        public Vector2 Size { get => size; set { size = value; dirty = true; } }

        public Rect actualRect;
        public Rect clippingRect;
        public bool dirty;
        public Vector2 LocalPosition { get => localPosition; set { localPosition = value; dirty = true; } }
    }
}

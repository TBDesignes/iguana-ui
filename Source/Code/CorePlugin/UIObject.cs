﻿using System;
using System.Collections.Generic;
using System.Linq;
using Duality;
using TBUI.Customs;


namespace TBUI {
    public class UIObject : Component, ICmpInitializable {
        private HashSet<UIObject> children = new HashSet<UIObject>();
        private HashSet<Custom> customs = new HashSet<Custom>();
        private UIObject parent;
        public IEnumerable<UIObject> Children => children;
        public List<Custom> Customs => customs.ToList();
        public Dictionary<string, object> Data { get; set; } = new Dictionary<string, object>();
        public UIObject Parent {
            get => parent;
            set {
                parent?.children.Remove(this);
                parent = value;
                parent?.children.Add(this);
            }
        }
        /// <summary>
        /// Read values from transform 
        /// </summary>
        public UITransform Transform { get => GameObj.GetComponent<UITransform>(); }

        private void GameObjEventParentChanged(object sender, GameObjectParentChangedEventArgs e) {
            if (Active) {
                if (e.NewParent != null && e.NewParent.GetComponent<UIObject>() is UIObject obj) {
                    Parent = obj;
                } else {
                    Parent = null;
                }
            }
        }

        private void CatchChildren() {
            foreach (var go in GameObj.Children) {
                if (go.GetComponent<UIObject>() is UIObject obj && obj.Active) {
                    obj.Parent = this;
                }
            }
        }

        public void OnActivate() {
            GameObj.EventParentChanged += GameObjEventParentChanged;
            Parent = GameObj.Parent?.GetComponent<UIObject>();
            CatchChildren();
        }

        public void OnDeactivate() {
            GameObj.EventParentChanged -= GameObjEventParentChanged;
            Parent = null;
        }

        public void AddCustom(Custom custom) => customs.Add(custom);
        public void RemoveCustom(Custom custom) => customs.Remove(custom);
        public void Drill<T>(Action<T> func) {
            DrillCustoms(func);
            foreach (var child in children) {
                child.Drill(func);
            }
        }
        public void DrillCustoms<T>(Action<T> func) {
            foreach (var c in customs) {
                if (c is T t) { func(t); }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Duality;

namespace TBUI
{
	/// <summary>
	/// Defines a Duality core plugin.
	/// </summary>
	public class TBUICorePlugin : CorePlugin
	{
		// Override methods here for global logic=
	}
}

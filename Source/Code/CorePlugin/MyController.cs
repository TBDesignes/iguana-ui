﻿using Duality;
using Duality.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBUI.Containers;

namespace TBUI {
    public class MyController : Component, ICmpInitializable, ICmpUpdatable{
        public SourceUI Source { get; set; }
        public ContentRef<Material> Material { get; set; }
        public ContentRef<Material> HoveredMaterial { get; set; }
        public void OnActivate() {
            if (Source != null) {
                Source.Root = new StackPanel();
                Source.Root.Transform.Anchor1 = new Vector2(0, 1);
                Source.Root.Transform.Anchor2 = Vector2.One;
                Source.Root.Transform.Size = new Vector2(150, 150);
                Source.Root.Transform.Offset = new Vector2(0, -75);
                Source.Root.Name = "Root";
                Source.Root.Style = new Style() { DefaultMaterial = Material, HoveredMaterial = HoveredMaterial };
                for (int i = 0; i < 0; i++) {
                    Node n = new Node();
                    n.Transform.Size = new Vector2(80, 80);
                    n.Parent = Source.Root;
                    n.Style = new Style() { DefaultMaterial = Material, HoveredMaterial = HoveredMaterial };
                    n.Name = "Button " + i;
                }
            }
        }

        public void OnDeactivate() {

        }

        public void OnUpdate() {
            if (DualityApp.Keyboard.KeyHit(Duality.Input.Key.Space)) {
                Node n = new Node();
                n.Transform.Size = new Vector2(80, 80);
                n.Parent = Source.Root;
                n.Name = "Additional button";
                n.Style = new Style() { DefaultMaterial = Material, HoveredMaterial = HoveredMaterial };
            }
            float speed = 100;
            if (DualityApp.Keyboard.KeyPressed(Duality.Input.Key.A))
                Source.Root.Transform.LocalPosition += new Vector2(-Time.DeltaTime * speed, 0);
            if (DualityApp.Keyboard.KeyPressed(Duality.Input.Key.D))
                Source.Root.Transform.LocalPosition += new Vector2(Time.DeltaTime * speed, 0);
            if (DualityApp.Keyboard.KeyPressed(Duality.Input.Key.W))
                Source.Root.Transform.LocalPosition += new Vector2(0, -Time.DeltaTime * speed);
            if (DualityApp.Keyboard.KeyPressed(Duality.Input.Key.S))
                Source.Root.Transform.LocalPosition += new Vector2(0, Time.DeltaTime * speed);
            if (DualityApp.Keyboard.KeyHit(Duality.Input.Key.E)) {
                Source.Root.Children.First().Enabled = !Source.Root.Children.First().Enabled;
            }
        }
    }
}

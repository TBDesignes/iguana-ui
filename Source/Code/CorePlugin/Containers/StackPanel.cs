﻿using Duality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBUI.Containers {
    public class StackPanel : Node {
        public bool Vertical { get; set; }
        public override void OnReconstruct() {
            base.OnReconstruct();
            Vector2 Transpone(Vector2 v) {
                if (Vertical)
                    return new Vector2(0, v.Y);
                else
                    return new Vector2(v.X, 0);
            }
            Vector2 offset = Vector2.Zero;
            foreach (var child in Children) {
                if (!child.Enabled) continue;
                child.Transform.LocalPosition = Transpone(offset);
                offset += Vertical ? new Vector2(0, child.Transform.Size.Y) : new Vector2(child.Transform.Size.X, 0);
                float maxWidth = Vertical ? child.Transform.Size.X : child.Transform.Size.Y;
            }
        }
    }
}

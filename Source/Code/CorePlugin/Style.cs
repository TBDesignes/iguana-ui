﻿using Duality;
using Duality.Drawing;
using Duality.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBUI {
    public class Style {
        public ContentRef<Material> DefaultMaterial { get; set; }
        public ContentRef<Material> HoveredMaterial { get; set; }
    }
}

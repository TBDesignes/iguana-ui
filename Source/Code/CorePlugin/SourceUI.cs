﻿using Duality;
using Duality.Components;
using Duality.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBUI {
    public class SourceUI : Component, ICmpRenderer, ICmpUpdatable, ICmpInitializable, ICmpEditorUpdatable {
        private Canvas canvas;
        private Node root;
        private Node hoveredNode;
        private Vector2 screenSize;
        public VisibilityFlag Visibility { get; set; }
        public Node Root {
            get => root; set {
                if (root != null)
                    root.Source = null;
                root = value;
                if (root != null)
                    root.Source = this;
            }
        }
        public void Draw(IDrawDevice device) {
            if (canvas == null)
                canvas = new Canvas();
            if (canvas.DrawDevice == null)
                canvas.Begin(device);
            Root?.Drill(node => node.OnDraw(canvas));
            if (canvas.DrawDevice != null)
                canvas.End();
        }

        public void GetCullingInfo(out CullingInfo info) {
            info.Radius = 1;
            info.Position = Vector3.Zero;
            info.Visibility = Visibility;
        }

        public void OnActivate() {

        }

        public void OnDeactivate() {

        }

        public void OnUpdate() {
            if (Root == null) return;
            if (screenSize != DualityApp.TargetViewSize) {
                screenSize = DualityApp.TargetViewSize;
                Root.Transform.dirty = true;
            }
            Root.Drill(node => node.OnUpdate());
            Vector2 cursosPos = DualityApp.Mouse.Pos;
            Node chain = hoveredNode;
            while (chain != null && !chain.Transform.clippingRect.Contains(cursosPos)) {
                chain.IsCursorHovering = false;
                chain.OnCursorHoverChanged(CURSOR_HOVER.Exited);
                chain = chain.Parent;
            }
            hoveredNode = null;
            if (Root.Enabled && chain == null && Root.Transform.clippingRect.Contains(cursosPos))
                chain = Root;
            while (chain != null) {
                if (!chain.IsCursorHovering) {
                    chain.IsCursorHovering = true;
                    chain.OnCursorHoverChanged(CURSOR_HOVER.Entered);
                }
                hoveredNode = chain;
                chain = chain.Children.FirstOrDefault(node => (node.Enabled && node.Transform.clippingRect.Contains(cursosPos)));
            }
        }
    }
}

﻿using Duality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBUI {
    [RequiredComponent(typeof(UIObject))]
    public class Custom : Component, ICmpAttachmentListener {
        private UIObject container;
        public UIObject Container {
            get => container;
            private set {
                container?.RemoveCustom(this);
                container = value;
                container?.AddCustom(this);
            }
        }
        public void OnAddToGameObject() {
            Container = GameObj.GetComponent<UIObject>();
        }

        public void OnRemoveFromGameObject() {
            Container = null;
        }
    }
}
